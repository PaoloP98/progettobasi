<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Registrati</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>
  <?php
   require("../partialpage/navbar.php");
    ?>
    <div class="login-clean">
        <form method="post" action ="../api/Utenti.php/Register">
            <h2 class="sr-only">Login Form</h2>
            <div class="illustration"><img src="assets/img/logo.png" style="width: 200px;"></div>
            <div class="form-group">
                <input required class="form-control" type="text" name="nome" placeholder="Nome">
            </div>
            <div class="form-group">
                <input required class="form-control" type="text" name="cognome" placeholder="Cognome">
            </div>
            <div class="form-group">
            <input required class="form-control" type="text" name="user" placeholder="Username" id="username">
                <div class="invalid-feedback">
                Username non disponibile.
                </div>
            </div>
            <div class="form-group">
                <input required class="form-control" type="email" name="email" placeholder="Email" id="email">
                <div class="invalid-feedback">
                    Email già utilizzata.
                </div>
            </div>
            <div class="form-group">
                <input required class="form-control" type="password" name="password" placeholder="Password">
            </div>
            <div class="form-group">
                <button disabled id="confermaRegistra" class="btn btn-primary btn-block" type="submit">Registrati</button>
            </div>
        </form>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
        $("#username,#email").on("keyup",function(){
            $.ajax({
                url: "../api/Utenti.php",
                type:"GET"
            }).then(function(data){
                console.log(data);
                var i = 0;
                var usertrovato = false;
                var emailtrovata = false;
                while(i<data.length ){
                    if($("#username").val() === data[i].username){
                        $("#username").addClass("is-invalid");
                        usertrovato = true;
                    }
                    if($("#email").val() === data[i].mail){
                        $("#email").addClass("is-invalid");
                        emailtrovata = true;
                    }
                    i++;
                }
                if(!usertrovato)
                    $("#username").removeClass("is-invalid");
                if(!emailtrovata)
                    $("#email").removeClass("is-invalid");
                var trovato = usertrovato || emailtrovata;
                console.log(trovato);
                $("#confermaRegistra").prop("disabled",trovato);

            });
        });
        });
    </script>
</body>

</html>