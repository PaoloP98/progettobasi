<?php
session_start();
require("connect.php");
?>

<html>
<head>
<title>Fudora</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="wwwroot/lib/bootstrap/css/bootstrap.min.css">
<link rel="icon" type="image/png" sizes="96x96" href="wwwroot/img/favicon.png">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="wwwroot/lib/bootstrap/js/bootstrap.min.js"></script>
 <link rel="stylesheet" href="//cdn.jsdelivr.net/bootstrap.tagsinput/0.4.2/bootstrap-tagsinput.css" />
 <script src="//cdn.jsdelivr.net/bootstrap.tagsinput/0.4.2/bootstrap-tagsinput.min.js"></script>

<link href="wwwroot/lib/tokenize/css/tokenize2.css" rel="stylesheet" />
<script src="wwwroot/lib/tokenize/js/tokenize2.js"></script>

<style>
@media screen and (min-width: 1000px) {
    .card-img-top {
        width: 100%;
        height: 15vw;
        object-fit: cover;
    }
}
@media screen and (max-width: 576px) {
    .card-img-top {
        width: 100%;
        height: 25vw !important;
        object-fit: cover;
    }
}


</style>
</head>
<body>
<?php
    require("partialpage/navbar.php");
?>

<div class="container">
  <div class="form-row mt-2">
    <div class="col-3">
      <select class="form-control" name="" id="tipoRicerca">
        <option value="nomeR">Ricerca per nome</option>
        <option value="ing">Ricerca per ingredienti</option>  
      </select>
    </div>
    <div class="col-9">
      <input type="text" class="form-control" id="nomeRicetta" placeholder="Nome della ricetta">
      <select class="i_ingredienti" multiple style="display:none;">
        <?php
            $query='SELECT id,nome,autore FROM ingredienti';
            $stmt = $con->prepare($query);
            $stmt->execute([]);
            while ($row = $stmt->fetch()) {
              echo "<option value='$row[0]'>$row[1]</option>";
            }
            ?>
          </select>
    </div>
    <div class="col">
      <select name="" class="form-control mt-2" id="sceltaCategoria">
        <option value=""> Selezionare la Categoria </option>
        <?php
          $stmt = $con->prepare('SELECT id,nome,foto FROM categorie');
          $stmt->execute([]);
          $stack=array();
          while ($row = $stmt->fetch()) {
              echo '<option value="'.$row[0].'">'.$row[1]."</option>";
        }
        ?>
        </option>
      </select>
    </div>
  </div>
  <div id="ricette" class="row">
  
  </div>
</div>


<script>
document.getElementById("tipoRicerca").selectedIndex = 0;
var urlParams = new URLSearchParams(window.location.search);
if(urlParams.has('categoria'))
  document.getElementById("sceltaCategoria").value=urlParams.get('categoria');

function loadRicette(){
    var i;  
    var url = "api/Ricette.php?q=" + $("#nomeRicetta").val();
    var cat = $("#sceltaCategoria").val() != ""? "&cat="+$("#sceltaCategoria").val() : "";
    var id = $(".i_ingredienti").val() != "" && $(".tokenize").is(":visible") ? "&id="+$(".i_ingredienti").val() : "";
    url += cat+id;
    $.ajax({
      url: url ,
      type:"GET"
      })
      .then(function(data){
        var out ="";
        for(i=0;i<data.length;i++){
          var fotoAlternativa = "this.src='wwwroot/img/categorie/"+data[i]["fotoCat"]+"'";
          out+=
          '<div class="col-4 py-2">'+
            '<div class="card-ricetta card border border-light shadow" data-idRicetta="'+data[i].id+'">'+
              '<img class="card-img-top img-fluid" onerror="'+ fotoAlternativa+'" src="'+window.location.origin+data[i].foto+'">'+
              '<div class="card-body">'+
                '<h5 class="card-title text-center">'+ data[i].nome+ '</h5>'+
              '</div>'+
            '</div>'+
          '</div>';
        }
        if(out === "")
          out = '<div class="col-12 mt-3 alert alert-primary" role="alert">Nessun risultato</div>';
        $("#ricette").html(out);
        })
  }
  loadRicette();

  $("#tipoRicerca").on("change",function(){
    var elementoScelto = $(this).val();
    if(elementoScelto === "nomeR"){
      $("#nomeRicetta").removeClass("d-none");
      $(".tokenize").addClass("d-none");
    }
    else if(elementoScelto ==="ing"){
      $("#nomeRicetta").addClass("d-none");
      $(".tokenize").removeClass("d-none");
    }
    loadRicette();
  });


  $('.i_ingredienti').tokenize2({
    dataSource: 'select',
    placeholder: 'Inserire i nomi degli ingredienti'
  });
  $(".tokenize").addClass("d-none");

  $("#nomeRicetta").on("keyup",function(){
    loadRicette();
  });
  $("#sceltaCategoria").on("change",function(){
    loadRicette();
  });


  $('.i_ingredienti').on('tokenize:tokens:add', function(e, value,text){
    loadRicette();
  });
  $('.i_ingredienti').on('tokenize:tokens:remove', function(e, value,text){
    loadRicette();
  });

   $(document).on("click",".card-ricetta",function(){
    var ricScelta = $(this).data("idricetta");
    window.location.href ="ricetta.php?id="+ricScelta;
  });
  
  
</script>
</body>
</html>
