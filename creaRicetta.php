<?php
require("connect.php");
session_start();
?>

<html>
<head>
<title>Fudora</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="wwwroot/lib/bootstrap/css/bootstrap.min.css">
<link rel="icon" type="image/png" sizes="96x96" href="wwwroot/img/favicon.png">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="wwwroot/lib/bootstrap/js/bootstrap.min.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css" integrity="sha256-39jKbsb/ty7s7+4WzbtELS4vq9udJ+MDjGTD5mtxHZ0=" crossorigin="anonymous" />

</head>
<body>
<?php
require("partialpage/navbar.php");
?>

<div class="container w-100">
  <form method="POST" action="api/Ricette.php"  enctype="multipart/form-data">
    <div class="form-row">
      <div class="col">
          <label class="font-weight-bold" for="nome">Nome ricetta</label>
          <input required type="text" class="form-control" id="nome" name="nome" placeholder="Nome della ricetta">
      </div>
      <div class="col">
        <label class="font-weight-bold" for="categoria">Categoria</label>
        <select required id="categoria" name="categoria" class="custom-select">
            <option selected value="">Selezionare la categoria</option>
        </select>
      </div>
    </div>
    <div class="form-row">
      <div class="col">
          <label class="font-weight-bold" for="porzioni">Numero porzioni</label>
          <input  min="1" required type="number" class="form-control" id="porzioni" placeholder="Numero porzioni" name="porzioni">
      </div>
      <div class="col">
          <label class="font-weight-bold" for="tempo">Tempo di preparazione</label>
          <input required type="time" class="form-control" id="tempo" placeholder="Tempo di preparazione" name="tempo">
      </div>
      <div class="col">
        <label class="font-weight-bold" for="difficolta">Difficoltà di preparazione</label>
        <select required id="difficolta" name="difficolta" class="custom-select">
            <option selected value="">Selezionare la difficoltà</option>
            <option value="1">Molto facile</option>
            <option value="2">Facile</option>
            <option value="3">Media</option>
            <option value="4">Difficile</option>
            <option value="5">Molto difficile</option>
        </select>
      </div>
    </div>

    <hr>
    <button type="button" id="addIng" class="btn btn-info my-1 w-100">Aggiungi ingrediente &nbsp; 
      <span style="font-size:16px; font-weight:bold;">+ </span>
    </button>
    <a href="#" data-toggle="modal" data-target="#exampleModalCenter">
      Non trovi un ingrediente? aggiungilo cliccando qui.
    </a>

      <div id="divIng" class="form-row">
        <div class="col-6">
          <input required type="text" id="nomeIngr-0" data-idy="0" class="form-control my-1 nomeIng" placeholder="Nome dell'ingrediente">
          <input type="hidden" id="hide-0" name="ingaggiunti[]">
        </div>
        <div class="col-6">
          <input required type="text" name="quantita[]"  class="form-control my-1" placeholder="Quantità">
        </div>          
      </div>

      <div class="col-12 p-0">
          <label class="font-weight-bold" for="testo">Descrivi il procedimento</label>
          <textarea required class="form-control" id="testo" rows="7" name="testo"></textarea>
      </div>
        
      <div class="p-4" style="position:relative;width:280px;">
        <label class="font-weight-bold">Foto della ricetta</label>
        <input type="file" class="d-none" accept="image/x-png,image/jpeg" name="fileToUpload" id="fileToUpload">
        <img src="wwwroot/img/logo.png" alt="ricetta" id="imgRicetta" style="max-height:200px;width:250px;">
        <label for="fileToUpload">
          <i class="fas fa-camera" style="position:absolute;bottom:20px;right:0px;font-size:2.5em;"></i>
        </label>
      </div>

      <button class="btn btn-success w-100 mt-4">Crea la ricetta</button>
</div>




</form>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Inserisci nuovo ingrediente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_nuovoIng">
          <div class="form-group">
              <label for="nome" class="sr-only">Nome ingrediente</label>
              <input type="text" class="form-control" name="nomeNuovoIng" id="nomeNuovoIng" placeholder="Nome ingrediente">
          </div>  
          <button type="submit" id="btnNuovoIng" class="btn btn-success w-100">Inserisci</button>
      </form>
      <script>

      $("#form_nuovoIng").submit(function(e) {

          e.preventDefault(); // avoid to execute the actual submit of the form.

          var form = $(this);

          $.ajax({
              url: "api/Ingredienti.php" ,
              type:"POST",
              dataType: "json",
              data: $("#form_nuovoIng").serialize()
              })
               $("#nomeNuovoIng").val("");
                $('#exampleModalCenter').modal('toggle');
      });
      </script>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>


  $(document).ready(function() {
    $.ajax( {
        url: "api/Categorie.php",
        type:"GET",
        success: function( data ) {
          var i;
          for( i = 0; i < data.length; i++){
            $("#categoria").append('<option value="'+data[i]["id"]+'">'+data[i]["nome"]+'</option>');
          }
        }
      });

    $("#fileToUpload").on("change",function(){
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imgRicetta')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(this.files[0]);
        }
    
    })

    var wrapper = $("#divIng");
    var add_button = $("#addIng");

    var x = 1;
    var id = 1;
    $(add_button).click(function(e) {
        e.preventDefault();
        x++;
        $(wrapper).append('<div class="row col-12" id="divInputIng-'+id+'"><div class="col-6 my-1">'+
          '<input required data-idy="'+id+'" id="nomeIngr-'+id+'" type="text" class="form-control nomeIng" placeholder="Nome dell\'ingrediente">'+
                              '<input type="hidden" id="hide-'+id+'" name="ingaggiunti[]">'+
                              '</div>'+
                            '<div class="col-5">'+
                            '<input required type="text" name="quantita[]"  class="form-control my-1" placeholder="Quantità">'+
                            '</div>'+
                            '<div class="col-1"><span data-id="'+id+'" class="input-group-addon transparent delete">'+
                              '<i class="fa fa-trash" aria-hidden="true" style="font-size: 2.2em;color:#ac0033;"></i></span></div>'+
                            '</div>'
                            );
        id++;

    });

    $(wrapper).on("click", ".delete", function(e) {
        e.preventDefault();
        $("#divInputIng-"+$(this).data("id")).remove();
        x--;
    })
});
$(document).on("keyup",".nomeIng",function(){
  var idInput = $(this).data("idy");
  var inputValue = $(this).val();
  $(this).autocomplete({
    select: function(event, ui){
      $(this).val(ui.item.label);
      $("#hide-"+idInput).val(ui.item.value);
      return false;
    },
  source: function( request, response ) {
    $.ajax( {
      url: "api/Ingredienti.php?q=" + inputValue,
      type:"GET",
      success: function( data ) {
        response( data );
      }
    });
  },
  change: function(event, ui) {
    findMatch($(this).val(),idInput);
  }
  });
});
function findMatch(inputValue,num) {
  $.ajax( {
      url: "api/Ingredienti.php?q=" + inputValue,
      type:"GET",
      success: function(data) {
        var matched = false;
        var i = 0;
        while(i < data.length && !matched){
          if (inputValue.toLowerCase() == data[i].label.toLowerCase()) {
            matched = true;
            $("#hide-"+num).val(data[i].value);
          }
          i++;
        }
        if(!matched){
          $("#hide-"+num).val("");
          $("#nomeIngr-"+num).val("");
          return;
          }
        }
    });
};

</script>
</body>
</html>

