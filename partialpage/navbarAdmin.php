<nav class="navbar navbar-expand-lg navbar-light bg-light">
  
  <a class="navbar-brand" href="../index.php">
    <img src="../wwwroot/img/logo.png" alt="Logo">
  </a>
  

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      
      <li class="nav-item active">
        <a class="nav-link" href="creaRicetta.php">Inserici Ricetta<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="adminpage.php">Area Admin<span class="sr-only">(current)</span></a>
      </li>
    </ul>
    <div class="navbar-collapse collapse w-100">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <?php
            if(!isset($_SESSION))
              session_start();
            echo $_SESSION["nome"].' '.$_SESSION["cognome"].' <a href="../logout.php">(Logout)</a>';
            ?>
        
        </li>
      </ul>
    </div>
  </div>
</nav>