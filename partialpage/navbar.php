<?php
  if(!isset($_SESSION))
    session_start();
  if(isset($_SESSION["userLogged"]) && $_SESSION["userLogged"]==true)
    require("navbarUtente.php");
  else if(isset($_SESSION["adminLogged"]) && $_SESSION["adminLogged"]==true)
    require("navbarAdmin.php");
  else
    require("navbarClassic.php");
?>