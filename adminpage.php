<?php
require("connect.php");
session_start();
if($_SESSION["adminLogged"] != 1)
  header("location:index.php");
?>
<html>
<head>
<title>Fudora</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="wwwroot/lib/bootstrap/css/bootstrap.min.css">
<link rel="icon" type="image/png" sizes="96x96" href="wwwroot/img/favicon.png">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="wwwroot/lib/bootstrap/js/bootstrap.min.js"></script>

</head>
<body>
<?php
require("partialpage/navbar.php");
?>
<div class="row px-4">
  <div class="col-3">
    <div class="list-group" id="list-tab" role="tablist">
      <a class="list-group-item list-group-item-action active" id="list-utenti-list" data-toggle="list" href="#list-utenti" role="tab" aria-controls="utenti">Gestione utenti</a>
      <a class="list-group-item list-group-item-action" id="list-ingredienti-list" data-toggle="list" href="#list-ingredienti" role="tab" aria-controls="ingredienti">Gestione ingredienti</a>
      <a class="list-group-item list-group-item-action" id="list-ricette-list" data-toggle="list" href="#list-ricette" role="tab" aria-controls="ricette">Gestione ricette</a>
    </div>
  </div>
  <div class="col-9">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="list-utenti" role="tabpanel" aria-labelledby="list-utenti-list">
      <table class="table table-striped">
        <thead>
          <tr>
            <th colspan="5" class="text-center table-primary">Admin</th>
          </tr>
          <tr>
          <th scope="col">Username</th>
            <th scope="col">Nome</th>
            <th scope="col">Cognome</th>
            <th scope="col">Email</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
        <?php
          $stmt = $con->prepare('SELECT nome,cognome,username,mail,admin FROM iscritti WHERE admin = true AND mail != :mail');
          $stmt->bindValue(':mail', $_SESSION["mail"]);
          $stmt->execute();
          $stack = array();
          while ($row = $stmt->fetch()) {
            $admin = FALSE;
            if($row["admin"] == 1)
            {
              $admin = TRUE;
            };
            echo '<tr>'.
              '<th scope="col">'.$row['username'].'</th>'.
              '<td>'.$row['nome'].'</td>'.
              '<td>'.$row['cognome'].'</td>'.
              '<td>'.$row['mail'].'</td>'. 
              '<td><select class="form-control" data-mail="'.$row['mail'].'" id="azioni-utente">'.
              '<option value="">Seleziona...</option>'.
              '<option value="user">Rendi user</option>'.
              '<option value="eliminaU">Elimina utente</option>'.
              '<option value="eliminaR">Elimina le ricette create dall\'utente</option>'.
              '<option value="eliminaI">Elimina gli ingredienti aggiunti dall\'utente</option>'.
              '</td>'.
          '</tr>';             
          '</tr>';
            }

        ?>


      <table class="table table-striped mt-5">
        <thead>
          <tr>
            <th colspan="5" class="table-info text-center">Utenti</th>
          </tr>
          <tr>
            <th scope="col">Username</th>
            <th scope="col">Nome</th>
            <th scope="col">Cognome</th>
            <th scope="col">Email</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
        <?php
          $stmt = $con->prepare('SELECT nome,cognome,username,mail,admin FROM iscritti WHERE admin = false');
          $stmt->execute([]);
          $stack = array();
          while ($row = $stmt->fetch()) {

            echo '<tr>'.
              '<th scope="col">'.$row['username'].'</th>'.
              '<td>'.$row['nome'].'</td>'.
              '<td>'.$row['cognome'].'</td>'.
              '<td>'.$row['mail'].'</td>'.
              '<td><select class="form-control" data-mail="'.$row['mail'].'" id="azioni-utente">'.
              '<option value="">Seleziona...</option>'.
              '<option value="admin">Rendi admin</option>'.
              '<option value="eliminaU">Elimina utente</option>'.
              '<option value="eliminaR">Elimina le ricette create dall\'utente</option>'.
              '<option value="eliminaI">Elimina gli ingredienti aggiunti dall\'utente</option>'.
              '</td>'.
          '</tr>';
            }

        ?>

        </tbody>
      </table>
        
      
      </div>
      <div class="tab-pane fade" id="list-ingredienti" role="tabpanel" aria-labelledby="list-ingredienti-list">
      <table class="table table-striped mt-5">
        <thead>
          <tr>
            <th colspan="5" class="table-info text-center">Ingredienti</th>
          </tr>
          <tr>
            <th scope="col">Nome</th>
            <th scope="col">autore</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
        <?php
          $stmt = $con->prepare('SELECT id,nome,autore FROM ingredienti ORDER BY nome');
          $stmt->execute([]);
          $stack = array();
          while ($row = $stmt->fetch()) {
            echo '<tr id="rigaIng-'.$row['id'].'">'.
              '<td id="ingrediente-'.$row['id'].'"><span>'.$row['nome'].'</span>'.
              '<input id="inputIng-'.$row['id'].'"class="form-control d-none" input="text" value="'.$row['nome'].'"></td>'.
              '<td>'.$row['autore'].'</td>'.
              '<td>'.
              '<button data-id="'.$row['id'].'" id="btn_modificaIng-'.$row['id'].'" class="btn btn-success w-100 mb-1 modificaIng">Modifica</button>'.
              '<button data-id="'.$row['id'].'" id="btn_eliminaIng-'.$row['id'].'" class="btn btn-danger w-100 eliminaIng">Elimina</button>'.
              '<button data-id="'.$row['id'].'" id="btn_salvaIng-'.$row['id'].'" class="btn btn-success d-none w-100 mb-1 salvaIng">Salva</button>'.
              '<button data-id="'.$row['id'].'" id="btn_annullaIng-'.$row['id'].'" class="btn btn-danger d-none w-100 annullaIng">Annulla</button>'.
              '</td>'.
          '</tr>';
            }

        ?>

        </tbody>
      </table>
      </div>
      <div class="tab-pane fade" id="list-ricette" role="tabpanel" aria-labelledby="list-ricette-list">
            <table class="table table-striped mt-5">
        <thead>
          <tr>
            <th colspan="5" class="table-info text-center">Ricette</th>
          </tr>
          <tr>
            <th scope="col"></th>
            <th scope="col">Nome</th>
            <th scope="col">Categoria</th>
            <th scope="col">Autore</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
        <?php
            $query = 'SELECT r.id as id,porzioni,difficolta,tempo,r.nome AS nomeR,i.username as autore,r.foto AS fotoR,testo,c.nome AS nomeCat, c.foto AS fotoc FROM ricette r JOIN categorie c ON r.categoria=c.id INNER JOIN iscritti i ON i.mail=r.autore';
            $stmt = $con->prepare($query);
                $stmt->execute();
            while($row = $stmt->fetch()){
              $id=$row['id'];
              $nome=$row['nomer'];
              $autore = $row['autore'];
              $foto = $row['fotor'];
              if($foto == "") 
                $foto = $row['fotoc'];
              $testo = $row['testo'];
              $categoria = $row['nomecat'];
              $porzioni = $row['porzioni'];
              $difficolta = $row['difficolta'];
              $tempo = $row['tempo'];
              $fotoAlternativa = "this.src='wwwroot/img/categorie/".$row['fotoc']."'";
              echo '<tr id="rigaRic-'.$id.'">'.
                '<td><img style="max-height: 70px;max-width: 70px;" src=".'.$foto.'" onerror="'.$fotoAlternativa.'"/></td>'.
                '<td><a href="ricetta.php?id='.$id.'">'.$nome.'</a></td>'.
                '<td>'.$categoria.'</td>'.
                '<td>'.$autore.'</td>'.
                '<td>'.
                '<button data-id="'.$id.'" id="btn_EliminaRic-'.$id.'" class="btn btn-danger w-100 eliminaRic">Elimina</button>'.
                '</td>'.
            '</tr>';
            }

        ?>

        </tbody>
      </table>
      </div>
    </div>
  </div>
</div>

<script>

$(document).on("click",".modificaIng",function(){
  var ingredienteScelto = $(this).data("id");
  console.log(ingredienteScelto);
  $("#ingrediente-"+ingredienteScelto + " span").addClass("d-none");
  $("#inputIng-"+ingredienteScelto).removeClass("d-none");
  $("#btn_modificaIng-"+ingredienteScelto).addClass("d-none");
  $("#btn_eliminaIng-"+ingredienteScelto).addClass("d-none");
  $("#btn_salvaIng-"+ingredienteScelto).removeClass("d-none");
  $("#btn_annullaIng-"+ingredienteScelto).removeClass("d-none");
});


$(document).on("click",".eliminaIng",function(){
  var ingredienteScelto = $(this).data("id");
  var r = confirm("Eliminare l'ingrediente?");
      if (r == true) {
        $.ajax({
          url: "api/Ingredienti.php?id="+ingredienteScelto ,
          type:"DELETE"
        }).then(function(){
          $("#rigaIng-"+ingredienteScelto).fadeOut("slow",function(){
            $("#rigaIng-"+ingredienteScelto).remove();
          });
        });
      }
});
$(document).on("click",".annullaIng",function(){
  var ingredienteScelto = $(this).data("id");
  $("#ingrediente-"+ingredienteScelto + " span").removeClass("d-none");
  $("#inputIng-"+ingredienteScelto).addClass("d-none");
  $("#btn_modificaIng-"+ingredienteScelto).removeClass("d-none");
  $("#btn_eliminaIng-"+ingredienteScelto).removeClass("d-none");
  $("#btn_salvaIng-"+ingredienteScelto).addClass("d-none");
  $("#btn_annullaIng-"+ingredienteScelto).addClass("d-none");
});
$(document).on("click",".salvaIng",function(){
  var ingredienteScelto = $(this).data("id");
    var ing = {
      id : ingredienteScelto,
      nome : $("#inputIng-"+ingredienteScelto).val() 
    }
    $.ajax({
        url: "api/Ingredienti.php/Aggiorna" ,
        type:"POST",
        dataType :"json",
        data: ing
      }).then(function(){
          $("#ingrediente-"+ingredienteScelto + " span").removeClass("d-none");
          $("#ingrediente-"+ingredienteScelto + " span").html($("#inputIng-"+ingredienteScelto).val() );
          $("#inputIng-"+ingredienteScelto).addClass("d-none");
          $("#btn_modificaIng-"+ingredienteScelto).removeClass("d-none");
          $("#btn_eliminaIng-"+ingredienteScelto).removeClass("d-none");
          $("#btn_salvaIng-"+ingredienteScelto).addClass("d-none");
          $("#btn_annullaIng-"+ingredienteScelto).addClass("d-none");
      });
});

  $(document).on("change","#azioni-utente",function(){
    var opzioneScelta = $(this).val();
    var mailUtente = $(this).data("mail");
    if(opzioneScelta==="admin"){
      var r = confirm("Rendere l'utente admin?");
      if (r == true) {
        var dati = {
          mail : mailUtente,
          admin:true,
        }
        $.ajax({
          url: "api/Utenti.php/Admin" ,
          type:"POST",
          dataType: "json",
          data: dati
        });
      }
    }
    else if(opzioneScelta === "user"){
      var r = confirm("Rimuove i privilegi di admin all'utente?");
      if (r == true) {
        var dati = {
          admin:false,
          mail : mailUtente,
        }
        $.ajax({
          url: "api/Utenti.php/Admin" ,
          type:"POST",
          dataType: "json",
          data: dati
        });
      }
    }
    else if(opzioneScelta === "eliminaU"){
      var r = confirm("Eliminare l'utente?");
      if (r == true) {
        $.ajax({
          url: "api/Utenti.php/"+mailUtente ,
          type:"DELETE"
        });
      }
    }
    else if(opzioneScelta === "eliminaR"){
      var r = confirm("Eliminare le ricette inserite dall'utente?");
      if (r == true) {
        $.ajax({
        url: "api/Ricette.php/"+mailUtente ,
        type:"DELETE"
      });
      }
    }
    else if(opzioneScelta === "eliminaI"){
      var r = confirm("Eliminare gli ingredienti inseriti dall'utente?");
      if (r == true) {
        $.ajax({
          url: "api/Ingredienti.php/"+mailUtente ,
          type:"DELETE"
        });
      }        
    }



  });

    $(document).on("click",".eliminaRic",function(){
  var ricettaScelta = $(this).data("id");
  var r = confirm("Eliminare la ricetta?");
      if (r == true) {
        $.ajax({
          url: "api/Ricette.php?id="+ricettaScelta ,
          type:"DELETE"
        }).then(function(){
          $("#rigaRic-"+ricettaScelta).fadeOut("slow",function(){
            $("#rigaRic-"+ricettaScelta).remove();
          });
        })
      }
});


</script>
</body>
</html>
