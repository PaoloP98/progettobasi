<?php
require("../connect.php");
session_start();

class Utenti {
    public $mail;
    public $nome;
    public $cognome;
    public $username;
    public $psw;
    public $admin;
    public function __construct($mail,$nome,$cognome,$username,$psw,$admin) {
        $this->mail = $mail;
        $this->nome = $nome;
        $this->cognome = $cognome;
        $this->username =$username;
        $this->psw = $psw;
        $this->admin = $admin;
    }
}
function returnData($arr) {
    header('Content-Type: application/json');
    echo json_encode($arr);
}

/*[HTTP GET api/Utenti.php]
Ottengo le informazioni sugli utenti
*/
if(!isset($_SERVER['PATH_INFO'])){
    if($_SERVER['REQUEST_METHOD'] == "GET"){
        $stmt = $con->prepare('SELECT * FROM iscritti');
        $stmt->execute([]);
        $stack = array();
        while ($row = $stmt->fetch()) {
            $utente = new Utenti($row[0],"","",$row[3],"","");
            array_push($stack,$utente);
        }        
        returnData($stack);
    }
   
}
else{
    /*
     [HTTP DELETE Utenti.php/mail@example.it]
     l'utente (admin) elimina un altro utente
    */
    $arrUrl = explode("/", substr($_SERVER['PATH_INFO'],1));
    if((isset($_SESSION["adminLogged"]) && $_SESSION["adminLogged"] == 1) &&
        count($arrUrl) === 1 &&
        filter_var($arrUrl[0], FILTER_VALIDATE_EMAIL)){
        if($_SERVER['REQUEST_METHOD'] == "DELETE") {
            $stmt = $con->prepare("DELETE FROM iscritti WHERE mail = :mail");
            $stmt->bindparam(":mail", $arrUrl[0]);
            $stmt->execute();
        }
    }
    /*
    [HTTP POST api/Utenti.php/Login]
    l'utente effettua il login al sito
    */
    else if(count($arrUrl) === 1 && $_SERVER['REQUEST_METHOD'] == "POST" && $arrUrl[0] == "Login"){
        $stmt = $con->prepare('SELECT * FROM Iscritti WHERE mail= :mail AND psw= :psw');
        $stmt->execute(['mail' => $_POST["email"], 'psw' =>sha1($_POST["password"]) ]); 

        if($row = $stmt->fetch()){            //login corretto
        print ($row[5]);
        if($row[5]==1){                       //admin
            $_SESSION["adminLogged"]=true;
            $_SESSION["userLogged"]=false;
        }else{                                   //utente  
            $_SESSION["userLogged"]=true;
            $_SESSION["adminLogged"]=false;
        }
        $_SESSION["nome"]=$row[1];
        $_SESSION["cognome"]=$row[2];
        $_SESSION["mail"]=$row[0];
        header("Location:http://" . $_SERVER['HTTP_HOST'] );

        }
        else{                                      //login errato
        $_SESSION["LoginErrato"]=true;
        header("Location:http://" . $_SERVER['HTTP_HOST'] . "/login");
        }
    }
    /*
    [HTTP POST api/Utenti.php/Register]
    l'utente si registra al sito ed i suoi dati vengono aggiunti al database
    */
    else if(count($arrUrl) === 1 && $arrUrl[0] == "Register")
    {
        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $nome = $_POST['nome'];
            $cognome = $_POST['cognome'];
            $mail  =$_POST['email'];
            $user  =$_POST['user'];
            $password  = sha1($_POST['password']);            
            try {
                $query = "INSERT INTO iscritti (mail,nome,cognome,username,psw,admin) VALUES (?, ?, ?, ?, ?, ?)";
                $stmt = $con->prepare($query);
                if (!$stmt) {
                    echo "\nPDO::errorInfo():\n";
                    print_r($pdo->errorInfo());
                }
                $stmt->execute(array($mail,$nome,$cognome,$user,$password,"FALSE"));
                echo " nome: " .$nome . " cognome: ".$cognome . " mail: " .$mail . " user: " .$user . " password: " . $password;
            }
            catch (PDOException $e){
                echo $e->getMessage();
            }

            $_SESSION["nome"]=$nome;
            $_SESSION["cognome"]=$cognome;
            $_SESSION["mail"]=$mail;
            $_SESSION["userLogged"]=true;
            header("Location:http://" . $_SERVER['HTTP_HOST'] );

        }
    }
    /*
    [HTTP POST api/Utenti.php/Admin]
    l'utente viene reso admin o utente in base al parametro 'admin' postato
    */
    else if(count($arrUrl) === 1 && $arrUrl[0] == "Admin" && $_SESSION["adminLogged"]==true){
        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $query = 'UPDATE iscritti '
                . 'SET admin = :admin '
                . 'WHERE mail = :mail';
            $admin = $_POST["admin"];
            $mail = $_POST["mail"];
            $stmt = $con->prepare($query);
            $stmt->bindValue(':admin', $admin);
            $stmt->bindValue(':mail', $mail);
            $stmt->execute();  
        }
    }
}
?>