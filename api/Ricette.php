<?php
require("../connect.php");
session_start();


class Ricetta {
    public $id;
    public $nome;
    public $testo;
    public $difficolta;
    public $porzioni;
    public $foto;
    public $categoria;
    public $autore;
    public $tempo;
    public $fotoCat;
    public function __construct($idR,$nomeR,$testoR,$difficoltaR,$porzioniR,$fotoR,$categoriaR,$autoreR,$tempo,$fotoCat) {
        $this->id = $idR;
        $this->nome = $nomeR;
        $this->testo = $testoR;
        $this->difficolta =$difficoltaR;
        $this->porzioni = $porzioniR;
        $this->foto = $fotoR;
        $this->categoria = $categoriaR;
        $this->autore = $autoreR;
        $this->tempo = $tempo;
        $this->fotoCat = $fotoCat;
    }

    public function jsonSerialize()
    {
        return $this;
    }
}
function returnData($arr) {
    header('Content-Type: application/json');
    echo json_encode($arr);
}
/*ottengo le informazioni su una ricetta applicando i filtri
[HTTP GET api/Ricette.php]
parametri:
    ?q = nome della ricetta
    ?cat = id della categoria
    ?id = id degli ingredienti che la ricetta deve avere (separati da ",")
*/
if(!isset($_SERVER['PATH_INFO'])){
    switch ($_SERVER['REQUEST_METHOD']) {
        case 'GET':
        $joinIng = "";
        $filtroIngredienti = "";
        $ing_arr;
        if(isset($_GET['id'])){
            $ing_arr = explode(",",$_GET['id']);
            $joinIng = " INNER JOIN contenuti c ON r.id = c.ricetta ";
            for($i = 0; $i < count($ing_arr); $i++){
                $filtroIngredienti .= " AND r.id IN " .
                " (SELECT ricetta FROM contenuti WHERE ingrediente = :ing".$i.") ";
            }
        }
        $query = 'SELECT DISTINCT r.id,r.nome,testo,difficolta,porzioni,r.foto,categoria,autore,tempo,cat.foto as fotocat '.
        'FROM ricette r '.$joinIng.' INNER JOIN categorie cat ON r.categoria = cat.id WHERE lower(r.nome) LIKE :nome ' . $filtroIngredienti;
        
        if(isset($_GET["cat"]) && $_GET["cat"] !="")
            $query .= "AND categoria=:categoria ";
        if(!isset($_GET['q']) || $_GET['q']== "")
            $nome = '%';
        else
            $nome = "%".strtolower ($_GET['q'])."%";
        
        $stmt = $con->prepare($query);
        $stmt->bindparam(":nome", $nome);
        if(isset($_GET['id'])){        
            for($i = 0; $i < count($ing_arr); $i++){
                $param = ":ing".$i;
                $stmt->bindparam($param, $ing_arr[$i]);
            }
        }
        if(isset($_GET["cat"]))
            $stmt->bindparam(":categoria", $_GET["cat"]);

        $stmt->execute();
        $stack=array();
        while ($row = $stmt->fetch()) {
            $ricetta = new Ricetta($row["id"],$row["nome"],$row["testo"],$row["difficolta"],$row["porzioni"],$row["foto"],$row["categoria"],$row["autore"],$row["tempo"],$row["fotocat"]);
            array_push($stack, $ricetta);
        }
        returnData($stack);
        break;

        /*
        l'utente inserisce una nuova ricetta
        [HTTP POST api/Ricette.php]
        */
        case 'POST':
            $nome = $_POST['nome'];
            $testo = $_POST['testo'];
            $difficolta = $_POST['difficolta'];
            $porzioni = $_POST['porzioni'];
            $categoria = $_POST['categoria'] ;
            $autore = $_SESSION['mail'];
            $ingredienti = $_POST['ingaggiunti'];
            $tempo = $_POST['tempo'];
            $quantita = $_POST["quantita"];
 
            $stmt = $con->prepare("INSERT INTO ricette (nome,testo,difficolta,porzioni,foto,categoria,autore,tempo)" 
                                ." VALUES(:nome,:testo,:difficolta,:porzioni,:foto,:categoria,:autore,:tempo)");
            $stmt->bindparam(":nome", $nome);
            $stmt->bindparam(":testo", $testo);
            $stmt->bindparam(":difficolta", $difficolta);
            $stmt->bindparam(":porzioni", $porzioni);
            $stmt->bindparam(":foto", $foto);
            $stmt->bindparam(":categoria", $categoria);
            $stmt->bindparam(":autore", $autore);
            $stmt->bindparam(":tempo", $tempo);
            $stmt->execute();
            
            $insertId = $con->lastInsertId();
            
            if(isset($_FILES['fileToUpload']) && $_FILES['fileToUpload']['error'] != UPLOAD_ERR_NO_FILE)
            {
                $userfile_tmp = $_FILES['fileToUpload']['tmp_name'];
                $userfile_name = $_FILES['fileToUpload']['name'];
                $estensione = explode(".", $userfile_name);
                $estensione = end($estensione);
                $uploaddir = "../wwwroot/img/ricette/";
                $userfile_name = $insertId . "." . $estensione;
                move_uploaded_file($userfile_tmp, $uploaddir . $userfile_name);
                $stmt = $con->prepare("UPDATE ricette set foto = :foto WHERE id = :id");
                $path =  "/wwwroot/img/ricette/" . $userfile_name;
                $stmt->bindparam(":foto", $path);
                $stmt->bindparam(":id", $insertId);
                $stmt->execute();
            }

            $stmt = $con->prepare("INSERT INTO contenuti(ricetta,ingrediente,quantita) VALUES (:ricetta,:ingrediente,:quantita)");
            for($i=0; $i<count($ingredienti);$i++){
                $stmt->bindparam(":ricetta", $insertId);
                $stmt->bindparam(":ingrediente", $ingredienti[$i]);
                $stmt->bindparam(":quantita", $quantita[$i]);
                $stmt->execute();
            }            

            header("Location:http://" . $_SERVER['HTTP_HOST'] ."/ricetta.php?id=".$insertId);            
            
            break;

        /*
        l'utente (admin) elimina una ricetta
        [HTTP DELETE]
        */
        case 'DELETE':
            if(isset($_SESSION["adminLogged"]) && $_SESSION["adminLogged"]==true){
                $id = $_GET["id"];
                $stmt = $con->prepare('SELECT foto FROM ricette r WHERE r.id= :idRicetta');
                $stmt->execute(['idRicetta' => $id]);
                $row = $stmt->fetch();
                $foto = $row["foto"];
                unlink(".." . $foto);
                $stmt = $con->prepare("DELETE FROM ricette " 
                                    ." WHERE id = :id");
                $stmt->bindparam(":id", $id);
                $stmt->execute();
            }
            break;
    }
   
}
else{
    $arrUrl = explode("/", substr($_SERVER['PATH_INFO'],1));

    //l'utente(admin) elimina le ricette di un altro utente
    //[DELETE : api/Ricette.php/mail@example.it]
    if(isset($_SESSION["adminLogged"]) && count($arrUrl) === 1 && filter_var($arrUrl[0], FILTER_VALIDATE_EMAIL)){
        if($_SERVER['REQUEST_METHOD'] == "DELETE") {
            $stmt = $con->prepare("DELETE FROM ricette WHERE autore = :mail");
            $stmt->bindparam(":mail", $arrUrl[0]);
            $stmt->execute();
        }
    }
}
    
?>