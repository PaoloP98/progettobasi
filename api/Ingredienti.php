<?php
require("../connect.php");
session_start();
class Ingredienti {
    public $value;
    public $label;
    public $autore;
    function __construct($id,$nome,$autore) {
        $this->value = $id;
        $this->label = $nome;
        $this->autore = $autore;
    }
}
function returnData($arr) {
    header('Content-Type: application/json');
    echo json_encode($arr);
}

if(!isset($_SERVER['PATH_INFO'])){
    switch ($_SERVER['REQUEST_METHOD']) {
        case 'GET':    
            $stmt = $con->prepare('SELECT id,nome,autore FROM ingredienti WHERE lower(nome) LIKE :nome');

            if(!isset($_GET['q']))
                $stmt->execute(['nome' => '%']);
            else
                $stmt->execute(['nome' => "%".strtolower ($_GET['q'])."%"]); 

            $stack=array();
            while ($row = $stmt->fetch()) {
                $ingrediente = new Ingredienti($row[0],$row[1],$row[2]);
                array_push($stack, $ingrediente);
            }
            returnData($stack);
        break;
        case 'POST':
            if(isset($_SESSION['mail'])){
                $nome = $_POST["nomeNuovoIng"];
                $mail = $_SESSION['mail'];
                $stmt = $con->prepare("INSERT INTO ingredienti (nome,autore) " 
                                    ." VALUES(:nome,:autore)");
                $stmt->bindparam(":nome", $nome);
                $stmt->bindparam(":autore", $mail);
                $stmt->execute();
            }
        break;
        /*
        [HTTP DELETE api/Ingredienti.php]
        l'utente (admin) elimina un ingrediente
        */
        case 'DELETE':
            if(isset($_SESSION["adminLogged"]) && $_SESSION["adminLogged"] == 1){
                $id = $_GET["id"];                
                $stmt = $con->prepare("DELETE FROM ingredienti " 
                                    ." WHERE id = :id");
                $stmt->bindparam(":id", $id);
                $stmt->execute();
            }
        break;
    }

}
else{
    $arrUrl = explode("/", substr($_SERVER['PATH_INFO'],1));
    /*
    [HTTP DELETE api/Ingredienti.php/mail@example.it]
    l'utente (admin) elimina tutti gli in ingredienti inseriti da un utente
    */
    if((isset($_SESSION["adminLogged"]) && $_SESSION["adminLogged"] == 1) &&
        count($arrUrl) === 1 && filter_var($arrUrl[0], FILTER_VALIDATE_EMAIL)){
        if($_SERVER['REQUEST_METHOD'] == "DELETE") {
            $stmt = $con->prepare("DELETE FROM ingredienti WHERE autore = :mail");
            $stmt->bindparam(":mail", $arrUrl[0]);
            $stmt->execute();
        }
    }
    /*
    [HTTP POST api/Ingredienti.php/Aggiorna]
    l'utente(admin) modifica il nome di un ingrediente
    */
    else if((isset($_SESSION["adminLogged"]) && $_SESSION["adminLogged"] == 1) &&
    $_SERVER['REQUEST_METHOD'] == "POST" && count($arrUrl) === 1 && $arrUrl[0] == "Aggiorna"){
        $stmt = $con->prepare("UPDATE ingredienti SET nome = :nome WHERE id = :id");
        $stmt->bindparam(":nome", $_POST['nome']);
        $stmt->bindparam(":id", $_POST['id']);
        $stmt->execute();
    }
}

?>