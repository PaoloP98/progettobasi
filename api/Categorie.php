<?php
require("../connect.php");
class Categorie {
    public $id;
    public $nome;
    public $foto;
    function __construct($id,$nome,$foto) {
        $this->id = $id;
        $this->nome = $nome;
        $this->foto = $foto;
    }
}
function returnData($arr) {
    header('Content-Type: application/json');
    echo json_encode($arr);
}

if(!isset($_SERVER['PATH_INFO'])){
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        $stmt = $con->prepare('SELECT id,nome,foto FROM categorie');
        $stmt->execute([]);

        $stack=array();
        while ($row = $stmt->fetch()) {
            $categoria = new Categorie($row[0],$row[1],$row[2]);
            array_push($stack, $categoria);
        }
        returnData($stack);
    }
}
?>