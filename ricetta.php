<?php
session_start();
require("connect.php");
?>

<html>
    <head>
        <title>Fudora</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="wwwroot/lib/bootstrap/css/bootstrap.min.css">
        <link rel="icon" type="image/png" sizes="96x96" href="wwwroot/img/favicon.png">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="wwwroot/lib/bootstrap/js/bootstrap.min.js"></script>
   
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css" integrity="sha256-39jKbsb/ty7s7+4WzbtELS4vq9udJ+MDjGTD5mtxHZ0=" crossorigin="anonymous" />

   
        <style type="text/css">
    .limit{
        width: 100%;
        height: 100%;
        overflow: hidden;
     }
    .limit img{
       max-width: 100%;
       max-height: 100%;
     }
 </style>
   
    </head>
    <body>
        <?php
            require("partialpage/navbar.php");
            $id=$_GET["id"];
            $query = 'SELECT r.id,c.id AS idcat,porzioni,difficolta,tempo,r.nome AS nomeR,i.username as autore,r.foto AS fotoR,testo,c.nome AS nomeCat,c.foto as fotocat FROM ricette r JOIN categorie c ON r.categoria=c.id INNER JOIN iscritti i ON i.mail=r.autore WHERE r.id=:id';
            $stmt = $con->prepare($query);
            $stmt->execute(['id' => $id]);
            $row = $stmt->fetch();
            $nome=$row['nomer'];
            $autore = $row['autore'];
            $foto = $row['fotor'];
            $testo = $row['testo'];
            $idcategoria = $row['idcat'];
            $categoria = $row['nomecat'];
            $porzioni = $row['porzioni'];
            $difficolta = $row['difficolta'];
            $tempo = $row['tempo'];
            $fotoAlternativa = "this.src='wwwroot/img/categorie/".$row['fotocat']."'";

            $ingredienti = array();
            $query = 'SELECT nome,quantita '.
            'FROM ingredienti i INNER JOIN contenuti c ON i.id=c.ingrediente '.
            'WHERE ricetta=:id';
            $stmt = $con->prepare($query);
            $stmt->execute(['id' => $id]);
            while($row = $stmt->fetch()){
                array_push($ingredienti,$row['nome'] ." " . $row['quantita']);
            }

        ?>
        
        <div class="row px-5 mt-3">
            <div class="col-12 mb-4">
                <a href="index.php?categoria=<?php echo $idcategoria; ?>" class="font-weight-bold float-left"><?php echo $categoria; ?></a>
                <span class="float-right">Ricetta aggiunta da <span class="font-weight-bold"><?php echo $autore; ?></span> </span>
            </div>
            <div class="col-12">
                <h1 class="display-3 mx-auto" style="width: 900px;"><?php echo $nome; ?></h1>
                <hr>
            </div>
            <div class="col-6">
                <div class="limit">
                    <img onerror="<?php echo $fotoAlternativa; ?>" src="<?php echo "http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . $foto; ?>"/>
                </div>
            </div>
            <div class="col-3">
                <h4>Ingredienti necessari:</h4>
                <ul>
                    <?php
                        foreach($ingredienti as $ingrediente){
                            echo "<li>".$ingrediente."</li>";
                        }
                    ?>
                </ul>
            </div>
            <div class="col-3">
                <h4>Dettagli ricetta:</h4>
                <?php
                    echo "<p> <i class='fas fa-user-friends'></i> Porzioni: ".$porzioni."</p>";
                    echo "<p><i class='far fa-clock'></i> Tempo necessario: ".$tempo."</p>";
                    echo "<p><i class='far fa-grin'></i> Difficoltà: ".$difficolta."</p>";
                ?>
            </div>

        </div>

        <div class="row mt-3 justify-content-center">
            <div class="col-10">
            <h4>Procedimento:</h4>
                <?php
                    echo "<p>".$testo."</p>";
                ?>
            </div>
        </div>
    </body>
</html>